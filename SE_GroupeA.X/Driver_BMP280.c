#include <xc.h>

#include "Driver_BMP280.h"
#include "TypesMacros.h"
#include "ConfigBits_P18F.h"

INT8U data_high = 0;
INT8U data_low = 0;

INT16U BMP280_Read(INT8U adr1)
{
    INT16U data = 0;
    //trame 1
    I2C_S();
    I2C_Write(SLAVE_ADDR_WRITE);   // x = 1 car (default) 0x77 SDO = VDDIO ( peut être à changer )
    I2C_ACKS();
    I2C_Write(adr1);
    I2C_ACKS();

    //trame 2
    I2C_RS();
    I2C_Write(SLAVE_ADDR_READ);      // x = 1 car (default) 0x77 SDO = VDDIO 
    I2C_ACKS();

    data_high = I2C_Read();

    I2C_ACKM();

    data_low = I2C_Read();
           
    //data |= I2C_Read();
    I2C_NACKM();
    I2C_P();
    
    return data;
}

float BMP280_GetTemp() {
    BMP280_Read(Temp_MSB);
    
    BMP280_S32_t adc_T = (data_high << 8) | data_low;
    adc_T = adc_T << 4;

    unsigned short dig_T1 = 27504;
    short dig_T2 = 26435;
    short dig_T3 = -1000;
    
    BMP280_S32_t var1, var2, T;
    var1 = ((((adc_T>>3) - ((BMP280_S32_t)dig_T1<<1))) * ((BMP280_S32_t)dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((BMP280_S32_t)dig_T1)) * ((adc_T>>4) - ((BMP280_S32_t)dig_T1))) >> 12) * 
    ((BMP280_S32_t)dig_T3)) >> 14;
    t_fine = var1 + var2;
    T = (t_fine * 5 + 128) >> 8;

    return (T / 10) / (float) 10;
}

BMP280_U32_t BMP280_GetPressure()
{
    BMP280_Read(Press_MSB);
    
    BMP280_S32_t adc_P = (data_high << 8) | data_low;
    adc_P = adc_P << 4;

    BMP280_S32_t var1, var2;
    BMP280_U32_t p;
    
    unsigned short dig_P1 = 36477;
    short dig_P2 = -10685;
    short dig_P3 = 3024;
    short dig_P4 = 2855;
    short dig_P5 = 140;
    short dig_P6 = -7;
    short dig_P7 = 15500;
    short dig_P8 = -14600;
    short dig_P9 = 6000;
            
    var1 = (((BMP280_S32_t)t_fine)>>1) - (BMP280_S32_t)64000;
    var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((BMP280_S32_t)dig_P6);
    var2 = var2 + ((var1*((BMP280_S32_t)dig_P5))<<1);
    var2 = (var2>>2)+(((BMP280_S32_t)dig_P4)<<16);
    var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((BMP280_S32_t)dig_P2) * var1)>>1))>>18;
    var1 =((((32768+var1))*((BMP280_S32_t)dig_P1))>>15);
    if (var1 == 0)
    {
        return 0; // avoid exception caused by division by zero
    }
    p = (((BMP280_U32_t)(((BMP280_S32_t)1048576)-adc_P)-(var2>>12)))*3125;
    if (p < 0x80000000) 
    {
        p = (p << 1) / ((BMP280_U32_t)var1);
    } 
    else
    {
        p = (p / (BMP280_U32_t)var1) * 2;
    }
    var1 = (((BMP280_S32_t)dig_P9) * ((BMP280_S32_t)(((p>>3) * (p>>3))>>13)))>>12;
    var2 = (((BMP280_S32_t)(p>>2)) * ((BMP280_S32_t)dig_P8))>>13;
    p = (BMP280_U32_t)((BMP280_S32_t)p + ((var1 + var2 + dig_P7) >> 4));
    
    return p / 100;
}

void BMP280_Init()
{
    I2C_Init();

    I2C_S();
    I2C_Write(SLAVE_ADDR_WRITE);

    I2C_ACKS();

    I2C_Write(ctrl_meas);
    I2C_ACKS();

    // osrs_t = 001 (16 bits)
    // osrs_p = 001 (16 bits)
    // mode = 11 (mode = normal)
    I2C_Write(0b00100111);
    I2C_ACKS();

    I2C_Write(config);
    I2C_ACKS();

    // t_sb = 101 (1s standby)
    // filter = 010 (2 samples)
    // reserverd ?
    // spi3w = 1 (3 wires ?)
    I2C_Write(0b10101001);
    I2C_ACKS();

    I2C_P();
}



