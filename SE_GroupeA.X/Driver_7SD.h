 #ifndef DRIVER_7SD_H
#define	DRIVER_7SD_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include "TypesMacros.h"
#include "Driver_BMP280.h"

const INT8U segment[12]= {
    0x3F,
    0x06,
    0x5B,
    0x4F,
    0x66,
    0x6D,
    0x7D,
    0x07,
    0x7F,
    0x6F,
    0x40,
    0x80
};

//unsigned int compteur, num,millier, centaine, dizaine, unite;  // Déclaration des variables

void SevenSegment_Clear();
void Sevensegment_init(void);

void displayDigit(int digit, int position);

void displayPressure(BMP280_U32_t number);
void displayTemperature(float number);



#endif

