#include <xc.h>
#include "ConfigBits_P18F.h"
#include "TypesMacros.h"
#include "Driver_7SD.h"
#include "Driver_BMP280.h"


void displayDigit(int digit, int position) {
    LATD = segment[digit]; // Envoyer les segments du chiffre à PORTD
    
    // Activer le digit correspondant via PORTD
    switch(position) {
        case 0: 
            PORTAbits.RA3 = 1; // Activer DIGIT1
            break;
        case 1:
            PORTAbits.RA2 = 1; // Activer DIGIT2
            break;
        case 2:
            PORTAbits.RA1 = 1; // Activer DIGIT3
            break;
        case 3:
            PORTAbits.RA0 = 1; // Activer DIGIT4
            break;
    }

    Delai_ms(1);  // Attendre un court moment (multiplexage rapide)
    
    // Désactiver le digit après l'affichage
    PORTA &= 0xF0;  // Désactiver tous les digits (RB0 à RB3 = 0)
}

void displayPressure(BMP280_U32_t number) {
    int digits[4];  // Pour stocker chaque chiffre du nombre

    // Décomposer le nombre en chiffres individuels
    digits[0] = (number / 1000) % 10;  // Milliers
    digits[1] = (number / 100) % 10;   // Centaines
    digits[2] = (number / 10) % 10;    // Dizaines
    digits[3] = number % 10;           // Unités

    // Balayer les 4 digits rapidement pour un effet multiplexé
    for (int i = 0; i < 4; i++) {
        displayDigit(digits[i], i);  // Afficher le chiffre correspondant
    }
}

void displayTemperature(float number){
    PORTAbits.RA1 = 1; // Activer DIGIT2
    LATD=0x80;
    if (number>0){
        number=number*10;
        displayPressure((BMP280_U32_t)number);
    }
    else{
        PORTAbits.RA3 = 1; // Activer DIGIT2
        LATD=0x40;
        number=number*10;
        int digits[4];  // Pour stocker chaque chiffre du nombre

    // Décomposer le nombre en chiffres individuels
    digits[1] = (-(int)number / 100) % 10;   // Centaines
    digits[2] = (-(int)number / 10) % 10;    // Dizaines
    digits[3] = -(int)number % 10;
    // Balayer les 4 digits rapidement pour un effet multiplexé
        for (int i = 1; i < 4; i++) {
            displayDigit(digits[i], i);  // Afficher le chiffre correspondant
        }
    }
}

void SevenSegment_Clear(void){
    LATD=0x00;
    TRISD=0;
    
}
void Sevensegment_init(void){
    TRISD = 0x00;
    TRISA = 0;
    SevenSegment_Clear();
}
