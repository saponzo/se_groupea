#include <xc.h>

#include "ConfigBits_P18F.h"

#include "Driver_I2C.h"
#include "TypesMacros.h"

void I2C_Init() {
    SCL_TRIS = 1;
    SDA_TRIS = 1;
    
    // SSPEN = 1 -> Enable serial port for SDA/SCL
    // SSPM3:0 = 1000 -> I2C Master mode
    SSPCON1 = 0b00101000;
    
    // SSPADD = 4 -> Fscl = 400khz
    SSPADD = 0b00000100;
}

void I2C_S() {
    SSPCON2bits.SEN = 1;
    while (SSPCON2bits.SEN) Delai_us(5);
}

void I2C_P() {
    SSPCON2bits.PEN = 1;
    while (SSPCON2bits.PEN) Delai_us(5);
}

void I2C_RS() {
    SSPCON2bits.RSEN = 1;
    while (SSPCON2bits.RSEN) Delai_us(5);
}

void I2C_ACKS() {
    while ((SSPSTATbits.R_W) || (SSPCON2bits.ACKSTAT)) Delai_us(5);
}

void I2C_ACKM() {
    SSPCON2bits.ACKDT = 0;
    SSPCON2bits.ACKEN = 1;
    while (SSPCON2bits.ACKEN) Delai_us(5);
}

void I2C_NACKM() {
    SSPCON2bits.ACKDT = 1;
    SSPCON2bits.ACKEN = 1;
    while (SSPCON2bits.ACKEN) Delai_us(5);
}

void I2C_Write(INT8U Value) {
    SSPBUF = Value; // Ecriture d'une donnee sur le SPI
    while (SSPSTATbits.BF) Delai_us(5); // Attendre la fin de la transmission
}

INT8U I2C_Read() {
    SSPCON2bits.RCEN = 1;
    while(!SSPSTATbits.BF) Delai_us(5); // Attendre la fin de la reception
    return SSPBUF; // Retourner la donnee recue
}