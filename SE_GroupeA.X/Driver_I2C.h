#ifndef DRIVER_I2C_H_
#define DRIVER_I2C_H_

#include "TypesMacros.h"

#define SCL_READ PORTCbits.RC3
#define SCL_WRITE LATCbits.RC3
#define SCL_TRIS TRISCbits.RC3

#define SDA_READ PORTCbits.RC4
#define SDA_WRITE LATCbits.RC4
#define SDA_TRIS TRISCbits.RC4

void I2C_Init(void);

// Motifs
// S Start
// P Stop
// ACKS ACK Slave
// ACKM ACK Master
// NACKM Not ACK Master
void I2C_S(void);
void I2C_P(void);
void I2C_RS(void);
void I2C_ACKS(void);
void I2C_ACKM(void);
void I2C_NACKM(void);

// Communication g�n�rale
void I2C_Write(INT8U Value);
INT8U I2C_Read(void);

#endif