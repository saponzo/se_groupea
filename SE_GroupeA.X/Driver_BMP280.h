#ifndef DRIVER_BMP280_H_
#define DRIVER_BMP280_H_

#include "TypesMacros.h"
#include "Driver_I2C.h"

#define BMP280_S32_t long int
#define BMP280_S64_t long long int

#define BMP280_U32_t unsigned long int
#define BMP280_U64_t unsigned long long int

//@ registre des pression
#define Press_MSB 0xF7

//@ registre des température
#define Temp_MSB 0xFA 

#define SLAVE_ADDR_WRITE 0b11101110
#define SLAVE_ADDR_READ  0b11101111


#define ctrl_meas 0xF4
#define config 0xF5



INT16U BMP280_Read(INT8U adr1);
void BMP280_Write(INT16U commande, INT8U adr1, INT8U adr2);
void BMP280_Init(void);

float BMP280_GetTemp(void);
BMP280_U32_t BMP280_GetPressure(void);

BMP280_S32_t t_fine;

#endif