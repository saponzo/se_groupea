#include <xc.h>
#include "ConfigBits_P18F.h"
#include "TypesMacros.h"
#include "Driver_I2C.h"
//#include "Driver_M24C08.h"
#include "Driver_BMP280.h"
#include "Driver_7SD.h"
#include "Driver_GLCD_S0108.h"
#include "Polices.h"

BOOLEAN IsPressure = TRUE; // affichage pression/temp
BMP280_U32_t pressure = 0;
float temp = 0;

// Fonction d'initialisation pour installer des interruptions
void IT_Init(void) {
    TRISBbits.RB0 = 1;
    INTCON2bits.INTEDG0 = 1; // INT 0 on rising edge
    INTCONbits.INT0IF = 0;   // INT 0 reset flag
    INTCONbits.INT0IE = 1;   // INT 0 enable

}

// Fonction d'autorisation globale
void IT_GlobalInterruptEnable(void) {
    INTCONbits.GIE = 1;
}

// Fonction de desactivation globale
void IT_GlobalInterruptDisable(void) {
    INTCONbits.GIE = 0;
}

// Routine d'interruption appelee lors d'une IT faite par Timer 0
void handleRB0(void) {
    INT0IF = 0;
    IsPressure = !IsPressure;
}

// Declaration du gestionnaire global d'interruptions
void __interrupt() InterruptManager(void) {
    if (INT0IE && INT0IF) {
        handleRB0();
    }
}

void main(void) {
    
    //Initialisation
    IT_Init();
    IT_GlobalInterruptEnable();

    Sevensegment_init();
    BMP280_Init();

    // Initialisation du GLCD
    GLCD_Init();
    
    // attente si certaines initialisations sont longues
    Delai_ms(1000);

    while(1) {
        
        //Affichage sur 7SD et/ou GLCD
        if (IsPressure == TRUE)
        {
            pressure = BMP280_GetPressure();
            
            displayPressure(pressure);
            GLCD_WriteAt(32,0,">",GLCD_WHITE);

        }
        else 
        {
            temp = BMP280_GetTemp();
            
            displayTemperature(temp);
            GLCD_WriteAt(0,0,">",GLCD_WHITE);

        }
        
        GLCD_WriteAt(0, 11, "Temperature", GLCD_WHITE);
        GLCD_WriteAt(32, 11, "Pression", GLCD_WHITE);
    }

    return;
}