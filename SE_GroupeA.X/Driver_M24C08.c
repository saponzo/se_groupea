
#include <xc.h>
#include "ConfigBits_P18F.h"
#include "TypesMacros.h"
#include "Driver_I2C.h"  // Include the I2C driver header
#include "Driver_M24C08.h"

#define EXT_EEPROM_I2C_ADDRESS 0x50  // Base address of the EEPROM (A2, A1, A0 grounded)

void M24C08_WriteByte(INT8U address, INT8U data) {
    
    I2C_S(); // D�marrer la communication I2C
    
    I2C_Write(EXT_EEPROM_I2C_ADDRESS << 1); // Envoyer l'adresse de l'EEPROM avec le bit d'�criture
    
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    I2C_Write(address); // Envoyer l'adresse m�moire o� �crire
    
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    
    I2C_Write(data); // Envoyer les donn�es � �crire
    
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    I2C_P(); // Arr�ter la communication I2C

    Delai_ms(5);  // Wait for the EEPROM to complete the write cycle
}

INT8U M24C08_ReadByte(INT8U address) {
    INT8U data;
    
    I2C_S(); // D�marrer la communication I2C
    I2C_Write(EXT_EEPROM_I2C_ADDRESS << 1); // Envoyer l'adresse de l'EEPROM avec le bit d'�criture
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    I2C_Write(address); // Envoyer l'adresse m�moire � lire
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    I2C_RS(); // Red�marrer la communication I2C
    I2C_Write((EXT_EEPROM_I2C_ADDRESS << 1) | 1); // Envoyer l'adresse de l'EEPROM avec le bit de lecture
    I2C_ACKS(); // Attendre l'ACK de l'EEPROM
    data = I2C_Read(); // Lire les donn�es et envoyer un NACK
    I2C_NACKM();
    I2C_P(); // Arr�ter la communication I2C

    return data;
}